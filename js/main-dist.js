"use strict";

!function() {
    var e = $("#quiz-form"), n = document.getElementById("answer-status"), s = document.getElementById("total-points"), t = document.getElementById("bal"), a = parseInt(t.innerHTML), i = 100, o = 21, r = 10, c = 5;
    e.validate({
        errorPlacement: function(e, n) {
            n.before(e);
        },
        rules: {
            confirm: {
                equalTo: "#password-2"
            }
        }
    });
    var u = [ "A", "B", "A" ];
    $("#questions").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: !0,
        labels: {
            finish: "View Total Points",
            next: "Next Question"
        },
        onFinished: function(n, s) {
            $(e).submit();
        },
        onStepChanging: function(d, l, g) {
            var L = l + 1, f = $("input[name='question-" + L + "-answers']:checked").val();
            return f ? (f === u[L] ? function() {
                n.innerHTML = "<p>correct!! You hav earned 10</p>", n.classList.remove("bg-danger"), 
                n.classList.add("bg-success"), s.innerHTML = i += r, t.innerHTML = a - o, console.log(a);
            }() : function() {
                n.innerHTML = "<p>you are wrong: You hav earned 5</p>", n.classList.remove("bg-success"), 
                n.classList.add("bg-danger"), s.innerHTML = i += c, t.innerHTML = a - o;
            }(), e.valid()) : (n.innerHTML = "<p>Please select an answer.</p>", n.classList.add("bg-danger"), 
            !1);
        }
    });
}();