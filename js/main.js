(function () {
    var form = $('#quiz-form');
    var answerStatus = document.getElementById('answer-status');
    var totalpoints = document.getElementById('total-points');
    var baldiv = document.getElementById('bal');
    var balance = parseInt(baldiv.innerHTML);
    var points = 100;
    var amountDeducted = 21;
    var correctPoints = 10;
    var wrongPoints = 5;

form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        confirm: {
            equalTo: "#password-2"
        }
    }
});

var answers = ['A', 'B', 'A'];

$("#questions").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    autoFocus: true,
    labels: {
        
        finish: "View Total Points",
        next: "Next Question"
    },

    onFinished: function (event, currentIndex)
    {
        $(form).submit();
    },
    onStepChanging: function (event, currentIndex, newIndex)
    {
        var currentQue = currentIndex + 1
        var answer = $("input[name='question-"+(currentQue)+"-answers']:checked").val() 
        //check if input is selected
          if(!answer)
          {
             answerStatus.innerHTML = "<p>Please select an answer.</p>"
             answerStatus.classList.add('bg-danger');
            return false;
          }

        // const correct = answers[currentQue].includes(answer);
        var expectedAnswer = answers[currentQue];
        if ( answer === expectedAnswer){
            handleCorrect()
        }else{
            handleWrong()
        }

        function handleCorrect() {
            answerStatus.innerHTML = "<p>correct!! You hav earned 10</p>"
            answerStatus.classList.remove('bg-danger');
            answerStatus.classList.add('bg-success');
            totalpoints.innerHTML= points+=correctPoints
            baldiv.innerHTML = (balance-amountDeducted);
            console.log(balance);
        }

        function handleWrong() {
             answerStatus.innerHTML = "<p>you are wrong: You hav earned 5</p>"
             answerStatus.classList.remove('bg-success');
             answerStatus.classList.add('bg-danger');
             totalpoints.innerHTML= points+=wrongPoints
            baldiv.innerHTML = (balance-amountDeducted);
        }

        return form.valid();
    },
});
}());